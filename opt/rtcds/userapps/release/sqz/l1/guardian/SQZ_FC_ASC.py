# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_FC.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/l1/guardian/SQZ_FC.py $

from guardian import GuardState, GuardStateDecorator
from guardian.state import TimerManager
#from guardian.ligopath import userapps_path
#from subprocess import check_output
import time
import cdsutils
import gpstime
import numpy as np
#import sys

# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

from ezca_automon import (
    EzcaEpochAutomon,
    EzcaUser,
    deco_autocommit,
    SharedState,
    DecoShared,
)

nominal = 'LOCKED'

# use a class with properties to access the RCG Epics-managed settings. If a module for setting is prefered like in the DRMI Guardian,
# then it is a drop-in replacement as both use attribute access
class FCParams(EzcaUser):
    UNLOCK_TIMEOUT_S = 1

    def __init__(self):
        self.timer = TimerManager(logfunc = None)
        self.timer['FAULT_TIMEOUT'] = 0
        self.timer['UNLOCK_TIMEOUT'] = 0
        self.timer['REDLIGHT_TIMEOUT'] = 0

    @deco_autocommit
    def update_calculations(self):
        None

    def GR_locked(self):
        return True

    def IR_locked(self):
        return True


class FCShared(SharedState):


    @DecoShared
    def fc(self):
        EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)
        return FCParams()


shared = FCShared()


##################################################
# Functions
##################################################
def down():
    for ii in range(4):
        dof = 'DOF%d'%(ii+1)

        # disable the LSC loop immediately and clear the history
        TRAMP = ezca['SQZ-FC_LSC_%s_TRAMP'%dof]
        ezca['SQZ-FC_LSC_%s_TRAMP'%dof] = 0
        ezca['SQZ-FC_LSC_%s_GAIN'%dof] = 0

        ezca['SQZ-FC_LSC_%s_RSET'%dof] = 2
        ezca['SQZ-FC_LSC_%s_TRAMP'%dof] = TRAMP

    for axis in ['CAV','INJ']:
        for dof1 in ['POS','ANG']:
            for dof2 in ['P','Y']:
                dof = '%s_%s_%s'%(axis,dof1,dof2)
                # disable the LSC loop immediately and clear the history
                TRAMP = ezca['SQZ-FC_ASC_%s_TRAMP'%dof]
                ezca['SQZ-FC_ASC_%s_TRAMP'%dof] = 0
                ezca['SQZ-FC_ASC_%s_GAIN'%dof] = 0

                ezca['SQZ-FC_ASC_%s_RSET'%dof] = 2
                ezca['SQZ-FC_ASC_%s_TRAMP'%dof] = TRAMP

def notify_log(msg):
    notify(msg)
    log(msg)


##################################################
# Decorator
##################################################
class GR_lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if not shared.fc.GR_locked():
            if shared.opo.timer['UNLOCK_TIMEOUT']:
                notify('Lost lock')
                return 'DOWN'
            notify_log('GR might lost lock...')
            return

        else:
            shared.fc.timer['UNLOCK_TIMEOUT'] = shared.fc.UNLOCK_TIMEOUT_S

class IR_lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if not shared.fc.IR_locked():
            if shared.opo.timer['UNLOCK_TIMEOUT']:
                notify('IR lost lock')
                return 'DOWN'
            notify_log('IR might lost lock...')
            return
        else:
            shared.fc.timer['UNLOCK_TIMEOUT'] = shared.fc.UNLOCK_TIMEOUT_S

##################################################
# STATES
##################################################
class INIT(GuardState):
    index = 0
    request = True

    def run(self):
        return True

class IDLE(GuardState):
    index = 1
    request = True

    def run(self):
        return True

class DOWN(GuardState):
    index = 2
    request = True
    goto = True

    def main(self):
        down()

    def run(self):
        return True

class GR_LOCKING(GuardState):
    index = 5
    request = True
    def main(self):
        self.counter = 0
        self.timer['wait'] = 0


    def run(self):
        if self.counter == 0:
            return True

class GR_LOCKED(GuardState):
    index = 10
    request = True
    def main(self):
        None

    @GR_lock_checker
    def run(self):
        if self.counter == 0:
            return True

class IR_LOCKING(GuardState):
    index = 50
    request = True
    def main(self):
        self.counter = 0
        self.timer['wait'] = 0


    def run(self):
        if self.counter == 0:
            return True
class IR_LOCKED(GuardState):
    index = 100
    request = True

    @IR_lock_checker
    def run(self):
        return True

class ASC_LOCKING(GuardState):
    index = 300
    request = True

    @IR_lock_checker
    def main(self):
        self.counter = 0
        self.timer['wait'] = 0

    @IR_lock_checker
    def run(self):
        return True

class LOCKED(GuardState):
    index = 1000
    request = True

    @IR_lock_checker
    def run(self):
        return True

edges = [
    ('INIT','IDLE'),
    ('DOWN','IDLE'),
    ('IDLE','GR_LOCKING'),
    ('GR_LOCKING','GR_LOCKED'),
    ('IDLE','IR_LOCKING'),
    ('IR_LOCKING','IR_LOCKED'),
    ('IR_LOCKED','ASC_LOCKING'),
    ('ASC_LOCKING','LOCKED'),
]

